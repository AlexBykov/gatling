# Gatling & Gitlab

[[_TOC_]]

## Basic
### 1. Simple job

```yaml
hello-job:
  stage: hello
  script:
    - echo "Hello"
```

### 2. Specify runner

```yaml
hello-job:
  stage: test
  script:
    - echo "Hello"
  tags:
    - load
```

### 3. Parametrize job

```yaml
hello-job:
  variables:
    NAME: Sergey
    AGE: 30
  stage: test
  script:
    - echo "Hello $NAME, $AGE"
  tags:
    - load
```

### 4. Stages

<details>
<summary>.gitlab-ci.yml</summary>

```yaml
variables:
  NAME: Noname

stages:
  - hello
  - test

hello-job:
  stage: hello
  script:
    - echo "Hello $NAME"
  tags:
    - debug

test-job-1:
  stage: test
  script:
    - echo "Hello $NAME"
  tags:
    - load

test-job-2:
  stage: test
  variables:
    NAME: Sergey
  script:
    - echo "Hello $NAME"
  tags:
    - debug
```

</details>

### 5. Extends

<details>
<summary>.gitlab-ci.yml</summary>

```yaml
variables:
  NAME: Noname

stages:
  - hello
  - test

.hello:
  script:
    - echo "Hello $NAME"
  tags:
    - debug

hello-job:
  stage: hello
  extends: .hello

test-job-1:
  stage: test
  extends: .hello

test-job-2:
  stage: test
  variables:
    NAME: Sergey
  extends: .hello
  tags:
    - load
```

</details>

### 6. Image

```yaml
hello-job:
  stage: test
  image: python
  script:
    - python3 --version
```

## Run Gatling

### 1. Local config

<details>
<summary>.gitlab-ci.yml</summary>

```yaml
image: hseeberger/scala-sbt:${JAVA_VERSION}_${SBT_VERSION}_${SCALA_VERSION}

stages:
  - test
  - report

variables:
  JAVA_VERSION: "17.0.2"
  SBT_VERSION: "1.6.2"
  SCALA_VERSION: "2.13.8"

  ORGANISATION: "ru.otus.load"
  SERVICE_NAME: "myservice"
  SIMULATION: "MaxPerformance"

  SBT_OPTS: "-Dconfig.override_with_env_vars=true"

test:
  stage: test
  script:
    - rm -rf target/gatling/*
    - echo "Runing simulation $ORGANISATION.$SERVICE_NAME.$SIMULATION"
    - sbt "Gatling / testOnly $ORGANISATION.$SERVICE_NAME.$SIMULATION"
  variables:
    JAVA_OPTS: "-Dgatling.charting.noReports=true"
  only:
    variables:
      - $ORGANISATION != ""
      - $SERVICE_NAME != ""
      - $SIMULATION != ""
  artifacts:
    when: always
    paths:
      - target/gatling/**/simulation.log
    expire_in: 1 days
  when: manual

pages:
  stage: report
  dependencies:
    - test
  script:
    - sbt "Gatling / generateReport"
    - mkdir -p public
    - cp -r target/gatling/**/*.html public/
    - cp -r target/gatling/**/js/ public/
    - cp -r target/gatling/**/style/ public/
    - rm -rf target/gatling/*
  artifacts:
    paths:
      - public
    expire_in: 14 days
  needs: ["test"]
  when: on_success
```

</details>

### 2. Remote config

```yaml
include:
  - project: 'otus-performance/ci-cd/lecture-2/template'
    ref: master
    file: '.pipeline.yml'
```

## References

- [Gitlab yaml reference](https://docs.gitlab.com/ee/ci/yaml/)
- [Gitlab includes](https://docs.gitlab.com/ee/ci/yaml/includes.html)
- [Gatling](https://github.com/gatling/gatling)
- [Gatling picatinny](https://github.com/TinkoffCreditSystems/gatling-picatinny)
- [Workshop](https://gitlab.com/tinkoffperfworkshop)
